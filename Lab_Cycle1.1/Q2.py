a=10
b=10.5
c='abc'
d=True
e=[1,2,"a"]
f={1,"John",45}
g=(1,"apple","orange")
h={"name":"abc","roll.no":24,"year":2021}
j=complex(a,b)
print("Datatype of " +str(a) +" is",type(a))
print("Datatype of "+str(b)+ " is",type(b))
print("Datatype of "+str(c)+" is ",type(c))
print("Datatype of "+str(d)+" is ",type(d))
print("Datatype of "+str(e)+" is ",type(e))
print("Datatype of "+str(f)+" is ",type(f))
print("Datatype of "+str(g)+" is ",type(g))
print("Datatype of "+str(h)+" is ",type(h))
print("Datatype of "+str(j)+" is ",type(j))
