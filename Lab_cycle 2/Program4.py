# Program that encrypts a message by adding a key value to every 
# character (Caesar Cipher) Hint : Say, if key = 3, then add 3 
# to every character. Use chr() and ord() functions

# Getting string to encrypt and key value from user
msg= input("Enter the string to be encrypted: ")
key = int(input("Enter the key value: "))

for letter in msg:
	
	if letter == " ":
		print(" ",end="")
	elif (ord(letter)+key)>ord("Z"):
		print(chr((ord(letter)+key)-26),end='')
		
	else:
		print(chr(ord(letter)+key),end='')
		
